/**
 * This is the main board, containing all pieces that
 * the players interact with. The board is static since
 * each game only has one instance of the board.
 * 
 * @author Daniel Kim
 * @author George Ding
 */

package board;

import java.util.ArrayList;

import color.*;
import pieces.*;


public class Board {
	
	/**
	 * <p>A static 2D array of <b>Piece</b>'s.</p>
	 */
	public static Piece[][] board;
	
	private static boolean gameStatus = false;

	/**
	 * <p>An ArrayList of <b>Piece</b>s that qualify as White pieces</p>
	 */
	public static ArrayList<Piece> whitepArray = new ArrayList<Piece>();
	
	/**
	 * <p>An ArrayList of <b>Piece</b>s that qualify as Black pieces</p>
	 */
	public static ArrayList<Piece> blackpArray = new ArrayList<Piece>();
	
	/**
	 * <p>A static white <b>King Piece</b>. There should only be one instance
	 * of this <b>Piece</b> on the board, hence it being static.
	 */
	public static King WhiteKing = new King(new White(), 'e', 1);
	
	/**
	 * <b>A static black <b>King Piece</b>. There should only be one instance
	 * of this <b>Piece</b> on the board, hence it being static.
	 */
	public static King BlackKing = new King(new Black(), 'e', 8);
	
	private Board() {};
	
	/**
	 * Gets the game's <em>current</em> status.
	 * @return true if game is still running
	 */
	public static boolean getGameStatus() {
		return gameStatus;
	}
	
	/**
	 * Updates the game's <em>current</em> status.
	 * @param status true if the game should be running, false otherwise.
	 */
	public static void setGameStatus(boolean status) {
		gameStatus = status;
	}
	
	/**
	 * Set's all pieces on the board and fills in each player's qualified pieces
	 */
	public static void setBoard() {
		gameStatus = true;
		board = new Piece[8][8];
		
		for(int i = 0; i < 8; i++) {
			
			board[1][i] = new Pawn(new Black(), ((char)('a' + i)), 7);
			board[6][i] = new Pawn(new White(), ((char)('a' + i)), 2);
			
			blackpArray.add(board[1][i]);
			whitepArray.add(board[6][i]);
		
		}
		
		board[0][0] = new Rook(new Black(), 'a', 8);
		board[0][1] = new Knight(new Black(), 'b', 8);
		board[0][2] = new Bishop(new Black(), 'c', 8);
		board[0][3] = new Queen(new Black(), 'd', 8);
		board[0][5] = new Bishop(new Black(), 'f', 8);
		board[0][6] = new Knight(new Black(), 'g', 8);
		board[0][7] = new Rook(new Black(), 'h', 8);
		
		for(int i = 0; i < 4; i++) {
			blackpArray.add(board[0][i]);
		}
		for(int i = 5; i < 8; i++) {
			blackpArray.add(board[0][i]);
		}
		
		board[0][4] = BlackKing;
		
		board[7][0] = new Rook(new White(), 'a', 1);
		board[7][1] = new Knight(new White(), 'b', 1);
		board[7][2] = new Bishop(new White(), 'c', 1);
		board[7][3] = new Queen(new White(), 'd', 1);
		board[7][5] = new Bishop(new White(), 'f', 1);
		board[7][6] = new Knight(new White(), 'g', 1);
		board[7][7] = new Rook(new White(), 'h', 1);
		
		for(int i = 0; i < 4; i++) {
			whitepArray.add(board[7][i]);
		}
		for(int i = 5; i < 8; i++) {
			whitepArray.add(board[7][i]);
		}
		
		board[7][4] = WhiteKing;
			
	}
	
	/**
	 * Gets the respective color's <b>King Piece</b>
	 * @param color Player's color. Uses Color instance.
	 * @return <b>WhiteKing</b> if color is <b>White</b>, returns <b>BlackKing</b> otherwise.
	 */
	public King getKing(Colors color){
		if(color instanceof White){
			return WhiteKing;
		}
		else{
			return BlackKing;
		}
	}
	
	/**
	 * Gets the alphabetical horizontal position given the numerical index position on the board
	 * @param hPos horizontal <b>int</b> position on the board
	 * @return char 'a' through 'h'; '\0' if <code>hPos < 0 || hPos > 7</code>
	 */
	public static char getHPos(int hPos) {
		if(hPos == 0) return 'a';
		
		if(hPos == 1) return 'b';
		
		if(hPos == 2) return 'c';
		
		if(hPos == 3) return 'd';
		
		if(hPos == 4) return 'e';
		
		if(hPos == 5) return 'f';
		
		if(hPos == 6) return 'g';
		
		if(hPos == 7) return 'h';
		
		return '\0';
	}
	
	/**
	 * Gets the numerical horizontal position given the alphabetical index position on the board
	 * @param hPos horizontal <b>char</b> position on the board
	 * @return int 0 though 7; -1 if hPos is not within 'a' through 'h' inclusive.
	 */
	public static int getHPos(char hPos) {
		if(hPos == 'a') {
			return 0;
		}
		else if(hPos == 'b') {
			return 1;
		}
		else if(hPos == 'c') {
			return 2;
		}
		else if(hPos == 'd') {
			return 3;
		}
		else if(hPos == 'e') {
			return 4;
		}
		else if(hPos == 'f') {
			return 5;
		}
		else if(hPos == 'g') {
			return 6;
		}
		else if(hPos == 'h') {
			return 7;
		}
		return -1;
	}
	
	public static int getVPos(int vPos) {
		return -1 * (vPos - 8);
	}
	
	/**
	 * Inserts the given piece in the give position on the board.
	 * <p>Supports Enpassant</p>
	 * <p>Identifies Check</p>
	 * <p>Toggles the King/Rook <b>moved</b> boolean for castling.
	 * 
	 * 
	 * @param piece <b>Piece</b> to move
	 * @param hPos horizontal <b>char</b> position on the board
	 * @param vert vertical position on the board.
	 */
	public static void insertPiece(Piece piece, char hPos, int vert) {
		//if(piece.movable(hor, vert) == true) {
		if(piece instanceof Pawn){
			if(((Pawn) piece).moved == false && Math.abs(piece.vertical - vert) == 2){
				((Pawn) piece).doublestep = true;
			}
			((Pawn) piece).moved = true;
		}
		else if(piece instanceof Rook){
			((Rook) piece).moved = true;
		}
		else if(piece instanceof King){
			((King) piece).moved = true;
			((King) piece).inCheck = false;
		}
		board[getVPos(piece.vertical)][getHPos(piece.horizontal)] = null;
		piece.horizontal = hPos;
		piece.vertical = vert;
		Piece locationPiece = board[getVPos(piece.vertical)][getHPos(piece.horizontal)];
		
		if(locationPiece != null) {
			if(locationPiece.isBlack()) {
				blackpArray.remove(locationPiece);
			}
			else {
				whitepArray.remove(locationPiece);
			}
		}
		
		board[getVPos(piece.vertical)][getHPos(piece.horizontal)] = piece;
		piece.checking = false;
		if(piece.isBlack()) {
			if(piece.movable(WhiteKing.horizontal, WhiteKing.vertical)) {
				piece.checking = true;
				WhiteKing.inCheck = true;
			}
		}
		else if(piece.isWhite()) {
			if(piece.movable(BlackKing.horizontal, BlackKing.vertical)) {
				piece.checking = true;
				BlackKing.inCheck = true;
			}
		}
		//}
	}
	
	/**
	 * Displays the <b>current</b> state of the board and each player's pieces
	 * using ASCII art.
	 */
	public static void displayBoard() {
		int a = 8;
		
		for(int row = 0; row < 8; row++) {
			for(int column = 0; column < 8; column++) {
				if(board[row][column] != null) {
					System.out.print(board[row][column].name + " ");
				}
				else if((row % 2 == 0) && (column % 2 == 0)) {
					System.out.print("   ");
				}
				else if((row % 2 != 0) && (column % 2 == 0)) {
					System.out.print("## ");
				}
				else if((row % 2 == 0) && (column %2 != 0)) {
					System.out.print("## ");
				}
				else {
					System.out.print("   ");
				}
			}
			System.out.print(a);
			a--;
			System.out.println();
		}
		System.out.print(" a  b  c  d  e  f  g  h");
		System.out.println();
	}
}
