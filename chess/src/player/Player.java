/**
 * Player class.
 * Players are able to move pieces and make moves.
 * Validity of those moves are checked in other classes.
 * 
 * @author Daniel Kim
 * @author George Ding
 */

package player;

import parser.Parser;
import pieces.Pawn;
import board.Board;
import color.*;
import key.Key;

public class Player {
	
	private String color;
	private String[] move;
	
	/**
	 * Constructor for <b>Player</b> instance.
	 * @param color Instance of <b>Colors</b>
	 */
	public Player(Colors color) {
		if(color instanceof Black) {
			this.color = "black";
		}else {
			this.color = "white";
		}
	}
	
	/**
	 * Player moves pieces on the board to desired position based on the <code>move</code> field.
	 * 
	 * @param key <b>Key</b> constant
	 * @return returns 1 if the move was valid, 0 if illegal
	 */
	public int makeMove(int key) {
		
		int[][] move = Parser.parseMove(this.move, key);
		
		if(Board.board[Board.getVPos(move[0][1])][(move[0][0])]
				.movable(Board.getHPos(move[1][0]), move[1][1])) {
			
			Board.insertPiece(Board.board[Board.getVPos(move[0][1])][(move[0][0])],
					Board.getHPos(move[1][0]), (move[1][1]));
			//*
			if(key == Key.CASTLE) {
				if(this.move[1].equals("g1")) {
					Board.insertPiece(Board.board[7][7], Board.getHPos(5), 1);
					return 1;
				}
				if(this.move[1].equals("c1")) {
					Board.insertPiece(Board.board[7][0], Board.getHPos(3), 1);
					return 1;
				}
				if(this.move[1].equals("g8")) {
					Board.insertPiece(Board.board[0][7], Board.getHPos(5), 8);
					return 1;
				}
				if(this.move[1].equals("c8")) {
					Board.insertPiece(Board.board[0][0], Board.getHPos(3), 8);
				}
			}
			//*/
			
			if(Board.board[Board.getVPos(move[1][1])][(move[1][0])] instanceof Pawn) {
				if(((Pawn) Board.board[Board.getVPos(move[1][1])][(move[1][0])]).reachedEnd()) {
					((Pawn) Board.board[Board.getVPos(move[1][1])][(move[1][0])]).promote(move[2][0]);
				}
			}
			
			return 1;
		}else {
			return 0;
		}
	}
	
	/**
	 * Sets the player move field to be parsed.
	 * @param move split string from user input
	 */
	public void setMove(String[] move) {
		this.move = move;
	}
	
	/**
	 * Gets the player move field to be parsed.
	 * @return <code> String[] move </code>
	 */
	public String[] getMove() {
		return move;
	}
	
	/**
	 * Gets the Player's <b>Colors</b> instance
	 * @return <b>Color</b> color
	 */
	public String getColor() {
		return color;
	}
	
	/**
	 * @return returns the Player's color as a String.
	 */
	public String toString() {
		return this.color.equals("white") ? "White":"Black";
	}
}
