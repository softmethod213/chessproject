/**
 * Superclass <b>Piece</b>
 * 
 * @author Daniel Kim
 * @author George Ding
 */

package pieces;

import color.Black;
import color.Colors;
import color.White;

public abstract class Piece implements PieceFunctions{
	
	/**
	 * Instance of <b>Colors</b>
	 */
	public Colors coloredPiece;
	
	/**
	 * Horizontal <b>char</b> value of the piece on the board
	 */
	public char horizontal;
	
	/**
	 * Vertical value of the piece on the board
	 */
	public int vertical;
	
	/**
	 * Name of the <b>Piece</b> instance based on color
	 */
	public String name;
	
	public boolean checking = false;
	
	/**
	 * Constructor for <b>Piece</b>
	 * 
	 * @param coloredPiece Instance of <b>Colors</b>
	 * @param horizontal Horizontal <b>char</b> value of the piece on the board
	 * @param vertical Vertical value of the piece on the board
	 */
	public Piece(Colors coloredPiece, char horizontal, int vertical) {
		this.coloredPiece = coloredPiece;
		this.horizontal = horizontal;
		this.vertical = vertical;
	}
	
	/**
	 * Determines if the <b>Piece</b> instance is a white <b>Colors</b> instance
	 * @return true if the instance is white, false otherwise
	 */
	public boolean isWhite() {
		if(this.coloredPiece instanceof Black) {
			return false;
		}
		else {
			return true;
		}
	}
	
	/**
	 * Determines if the <b>Piece</b> instance is a black <b>Colors</b> instance
	 * @return true if the instance is black, false otherwise
	 */
	public boolean isBlack() {
		if(this.coloredPiece instanceof Black) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Determines this instance is the same color as the parameter
	 * @param color Instance of <b>Colors</b>
	 * @return true if the parameter instance is the same as the <b>Piece</b>'s <b>Colors</b>
	 * instance, false otherwise.
	 */
	public boolean sameColor(Colors color){
		if(this.isBlack()){
			if(color instanceof Black){
				return true;
			}
		}
		else if(this.isWhite()){
			if(color instanceof White){
				return true;
			}
		}
		return false;
	}
	
}
