/**
 * <b>Rook</b> subclass of <b>Piece</b>
 * @author Daniel Kim
 * @author George Ding
 */

package pieces;

import board.Board;
import color.Black;
import color.Colors;

public class Rook extends Piece implements PieceFunctions{
	
	/**
	 * True if the Rook has previously been moved, false otherwise. 
	 */
	public boolean moved = false;

	/**
	 * Constructor for <b>Rook</b> instance
	 * 
	 * @param coloredPiece An instance of <b>Colors</b>
	 * @param horizontal Initial horizontal <b>char</b> position of the piece
	 * @param vertical Initial vertical position of the piece
	 */
	public Rook(Colors coloredPiece, char horizontal, int vertical) {
		super(coloredPiece, horizontal, vertical);
		if(this.coloredPiece instanceof Black) {
			this.name = "bR";
		}
		else {
			this.name = "wR";
		}
	}

	@Override
	/**
	 * Overrided implementation of movable method from <b>PieceFunctions</b> for <b>Rook</b>
	 */
	public boolean movable(char hor, int vert) {
		if(hor == this.horizontal && vert == this.vertical){
			return false;
		}
		
		int verticalDest = Board.getVPos(vert);
		int horizontalDest = Board.getHPos(hor);
		int verticalLoc = Board.getVPos(this.vertical);
		int horizontalLoc = Board.getHPos(this.horizontal);
		
		if(hor == this.horizontal){
			Piece current = Board.board[verticalLoc][horizontalLoc];
			
			while(verticalLoc != verticalDest){
				if(verticalLoc > verticalDest){
					verticalLoc--;
				}
				else{
					verticalLoc++;
				}
				current = Board.board[verticalLoc][horizontalLoc];
				if(current != null && verticalLoc != verticalDest){
					return false;
				}
			}
			if(current == null || !current.sameColor(this.coloredPiece)){
				return true;
			}
		}
		else if(vert == this.vertical){
			Piece current = Board.board[verticalLoc][horizontalLoc];
			
			while(horizontalLoc != horizontalDest){
				if(horizontalLoc > horizontalDest){
					horizontalLoc--;
				}
				else{
					horizontalLoc++;
				}
				
				current = Board.board[verticalLoc][horizontalLoc];
				if(current != null && horizontalLoc != horizontalDest){
					return false;
				}
			}
			if(current == null || !current.sameColor(this.coloredPiece)){
				return true;
			}
		}
		
		return false;
	}
}


