/**
 * Functional interface for determining <b>Piece</b> movement validity
 * 
 * @author Daniel Kim
 * @author George Ding
 */

package pieces;

public interface PieceFunctions {
	/**
	 * Abstract method for determining <b>Piece</b> movement validity
	 * @param hor Horizontal <b>char</b> value of the piece on the board
	 * @param vert Vertical value of the piece on the board
	 * @return true if movable, false otherwise
	 */
	public boolean movable(char hor, int vert);
}
