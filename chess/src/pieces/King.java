/**
 * <b>King</b> subclass of <b>Piece</b>
 * @author Daniel Kim
 * @author George Ding
 */

package pieces;

import board.Board;
import color.Black;
import color.Colors;

public class King extends Piece implements PieceFunctions{
	
	/**
	 * True if the King has previously been moved, false otherwise
	 */
	public boolean moved = false;
	
	/**
	 * True if the King is currently checked, false if not.
	 */
	public boolean inCheck = false;

	/**
	 * Constructor for <b>King</b> instance
	 * 
	 * @param coloredPiece An instance of <b>Colors</b>
	 * @param horizontal Initial horizontal <b>char</b> position of the piece
	 * @param vertical Initial vertical position of the piece
	 */
	public King(Colors coloredPiece, char horizontal, int vertical) {
		super(coloredPiece, horizontal, vertical);
		if(this.coloredPiece instanceof Black) {
			this.name = "bK";
		}
		else {
			this.name = "wK";
		}
	}

	@Override
	/**
	 * Overrided implementation of movable method from <b>PieceFunctions</b> for <b>King</b>
	 * Supports castling and invalid moves if checked/checkmated
	 */
	public boolean movable(char hor, int vert) {
		if(hor == this.horizontal && vert == this.vertical){
			return false;
		}
		else if(Board.board[Board.getVPos(vert)][Board.getHPos(hor)] != null && Board.board[Board.getVPos(vert)][Board.getHPos(hor)].sameColor(this.coloredPiece)){
			return false;
		}
		
		int curHor = Board.getHPos(this.horizontal);
		int curVert = Board.getVPos(this.vertical);
		
		int horLoc = Board.getHPos(hor);
		int vertLoc = Board.getVPos(vert);
		
		if(this.isBlack()) {
			for(int i = 0; i < Board.whitepArray.size(); i++) {
				Piece tempPiece = Board.board[Board.getVPos(vert)][Board.getHPos(hor)];
				if(Board.whitepArray.get(i) instanceof Pawn) {
					Board.board[Board.getVPos(vert)][Board.getHPos(hor)] = new King(this.coloredPiece, hor, vert);
				}
				if(Board.whitepArray.get(i).movable(hor, vert)) {
					Board.board[Board.getVPos(vert)][Board.getHPos(hor)] = tempPiece;
					return false;
				}
				Board.board[Board.getVPos(vert)][Board.getHPos(hor)] = tempPiece;
			}
		}
		else {
			for(int i = 0; i < Board.blackpArray.size(); i++) {
				Piece tempPiece = Board.board[Board.getVPos(vert)][Board.getHPos(hor)];
				if(Board.blackpArray.get(i) instanceof Pawn) {
					Board.board[Board.getVPos(vert)][Board.getHPos(hor)] = new King(this.coloredPiece, hor, vert);
				}
				if(Board.blackpArray.get(i).movable(hor, vert)) {
					Board.board[Board.getVPos(vert)][Board.getHPos(hor)] = tempPiece;
					return false;
				}
				Board.board[Board.getVPos(vert)][Board.getHPos(hor)] = tempPiece;
			}
		}
		
		if(this.isBlack()) {
			if(!this.inCheck) {
				if((hor == 'g' && vert == 8) && (Board.board[0][7] instanceof Rook) &&
						(this.moved == false && ((Rook)Board.board[0][7]).moved == false) &&
							(Board.board[0][6] == null && Board.board[0][5] == null)) {
						
					return true;
				}else if((hor == 'c' && vert == 8) && (Board.board[0][0] instanceof Rook) &&
						(this.moved == false && ((Rook)Board.board[0][0]).moved == false) &&
							(Board.board[0][3] == null && Board.board[0][2] == null)){
				
					return true;
				}
			}
		}else {
			if(!this.inCheck) {
				if((hor == 'g' && vert == 1) && (Board.board[7][7] instanceof Rook) &&
						(this.moved == false && ((Rook)Board.board[7][7]).moved == false) &&
							(Board.board[7][6] == null && Board.board[7][5] == null)) {
						
					return true;
				}else if((hor == 'c' && vert == 1) && (Board.board[7][0] instanceof Rook) &&
						(this.moved == false && ((Rook)Board.board[7][0]).moved == false) &&
							(Board.board[7][3] == null && Board.board[7][2] == null)){
				
					return true;
				}
			}
		}
		
		if(horLoc - 1 == curHor && vertLoc == curVert){
			if(Board.board[vertLoc][horLoc] == null || !Board.board[vertLoc][horLoc].sameColor(this.coloredPiece)){
				return true;
			}
		}
		else if(horLoc + 1 == curHor && vertLoc == curVert){
			if(Board.board[vertLoc][horLoc] == null || !Board.board[vertLoc][horLoc].sameColor(this.coloredPiece)){
				return true;
			}
		}
		else if(horLoc == curHor && vertLoc + 1 == curVert){
			if(Board.board[vertLoc][horLoc] == null || !Board.board[vertLoc][horLoc].sameColor(this.coloredPiece)){
				return true;
			}
		}
		else if(horLoc == curHor && vertLoc - 1 == curVert){
			if(Board.board[vertLoc][horLoc] == null || !Board.board[vertLoc][horLoc].sameColor(this.coloredPiece)){
				return true;
			}
		}
		return false;
	}
}
//a