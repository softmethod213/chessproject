/**
 * <b>Queen</b> subclass of <b>Piece</b>
 * <b>Bishop</b> and <b>Rook</b> movements are combined to create Queen movements
 * @author Daniel Kim
 * @author George Ding
 */

package pieces;

import color.Black;
import color.Colors;
import pieces.Bishop;
import pieces.Rook;

public class Queen extends Piece implements PieceFunctions{
	
	Bishop bishopmovement;
	Rook rookmovement;
	
	/**
	 * Constructor for <b>Queen</b> instance
	 * 
	 * @param coloredPiece An instance of <b>Colors</b>
	 * @param horizontal Initial horizontal <b>char</b> position of the piece
	 * @param vertical Initial vertical position of the piece
	 */
	public Queen(Colors coloredPiece, char horizontal, int vertical) {
		super(coloredPiece, horizontal, vertical);
		if(this.coloredPiece instanceof Black) {
			this.name = "bQ";
		}
		else {
			this.name = "wQ";
		}
	}

	@Override
	/**
	 * Overrided implementation of movable method from <b>PieceFunctions</b> for <b>Queen</b>
	 */
	public boolean movable(char hor, int vert) {
		if(hor == this.horizontal && vert == this.vertical){
			return false;
		}
		
		bishopmovement = new Bishop(this.coloredPiece, this.horizontal, this.vertical);
		rookmovement = new Rook(this.coloredPiece, this.horizontal, this.vertical);
		
		if(bishopmovement.movable(hor, vert) || rookmovement.movable(hor, vert)){
			return true;
		}
		return false;
	}
	
}
