/**
 * <b>Knight</b> subclass of <b>Piece</b>
 * @author Daniel Kim
 * @author George Ding
 */

package pieces;

import board.Board;
import color.Black;
import color.Colors;

public class Knight extends Piece implements PieceFunctions{
	
	/**
	 * Constructor for <b>Knight</b> instance
	 * 
	 * @param coloredPiece An instance of <b>Colors</b>
	 * @param horizontal Initial horizontal <b>char</b> position of the piece
	 * @param vertical Initial vertical position of the piece
	 */
	public Knight(Colors coloredPiece, char horizontal, int vertical) {
		super(coloredPiece, horizontal, vertical);
		if(this.coloredPiece instanceof Black) {
			this.name = "bN";
		}
		else {
			this.name = "wN";
		}
	}

	@Override
	/**
	 * Overrided implementation of movable method from <b>PieceFunctions</b> for <b>Knight</b>
	 */
	public boolean movable(char hor, int vert) {
		if(hor == this.horizontal && vert == this.vertical){
			return false;
		}
		
		int curHor = Board.getHPos(this.horizontal);
		int curVert = Board.getVPos(this.vertical);
		
		int horLoc = Board.getHPos(hor);
		int vertLoc = Board.getVPos(vert);
		
		if(Math.abs(curHor - horLoc) == 2 && Math.abs(curVert - vertLoc) == 1){
			if(Board.board[vertLoc][horLoc] == null || !Board.board[vertLoc][horLoc].sameColor(this.coloredPiece)){
				return true;
			}
		}
		else if(Math.abs(curHor - horLoc) == 1 && Math.abs(curVert - vertLoc) == 2){
			if(Board.board[vertLoc][horLoc] == null || !Board.board[vertLoc][horLoc].sameColor(this.coloredPiece)){
				return true;
			}
		}
		return false;
	}

}
