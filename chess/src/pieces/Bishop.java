/**
 * <b>Bishop</b> subclass of <b>Piece</b>
 * @author Daniel Kim
 * @author George Ding
 */

package pieces;

import board.Board;
import color.Black;
import color.Colors;

public class Bishop extends Piece implements PieceFunctions{
	/**
	 * Constructor for <b>Bishop</b> instance
	 * 
	 * @param coloredPiece An instance of <b>Colors</b>
	 * @param horizontal Initial horizontal <b>char</b> position of the piece
	 * @param vertical Initial vertical position of the piece
	 */
	public Bishop(Colors coloredPiece, char horizontal, int vertical) {
		super(coloredPiece, horizontal, vertical);
		if(this.coloredPiece instanceof Black) {
			this.name = "bB";
		}
		else {
			this.name = "wB";
		}
	}

	@Override
	/**
	 * Overrided implementation of movable method from <b>PieceFunctions</b> for <b>Bishop</b>
	 */
	public boolean movable(char hor, int vert) {
		if(hor == this.horizontal && vert == this.vertical){
			return false;
		}
		
		int verticalDest = Board.getVPos(vert);
		int horizontalDest = Board.getHPos(hor);
		int verticalLoc = Board.getVPos(this.vertical);
		int horizontalLoc = Board.getHPos(this.horizontal);
		
		if(Math.abs(verticalDest - verticalLoc) != Math.abs(horizontalDest - horizontalLoc)){
			return false;
		}
		else{
			Piece current = Board.board[verticalLoc][horizontalLoc];
			
			while(verticalLoc != verticalDest){
				if(verticalLoc > verticalDest){
					verticalLoc--;
				}
				else{
					verticalLoc++;
				}
				if(horizontalLoc > horizontalDest){
					horizontalLoc--;
				}
				else{
					horizontalLoc++;
				}
				
				current = Board.board[verticalLoc][horizontalLoc];
				if(current != null && verticalLoc != verticalDest){
					return false;
				}
			}
			
			if(current == null || !current.sameColor(this.coloredPiece)){
				return true;
			}
		}
		return false;
	}
}
