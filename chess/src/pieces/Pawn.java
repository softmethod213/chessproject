/**
 * <b>Pawn</b> subclass of <b>Piece</b>
 * @author Daniel Kim
 * @author George Ding
 */

package pieces;

import board.Board;
import color.Black;
import color.Colors;
import key.Key;

public class Pawn extends Piece implements PieceFunctions{
	
	/**
	 * True if the Pawn has previously been moved, false otherwise.
	 */
	public boolean moved = false;
	
	/**
	 * True if the Pawn has move two spaces previously, false otherwise.
	 */
	public boolean doublestep = false;
	
	/**
	 * Constructor for <b>Pawn</b> instance
	 * 
	 * @param coloredPiece An instance of <b>Colors</b>
	 * @param horizontal Initial horizontal <b>char</b> position of the piece
	 * @param vertical Initial vertical position of the piece
	 */
	public Pawn(Colors coloredPiece, char horizontal, int vertical) {
		super(coloredPiece, horizontal, vertical);
		if(this.coloredPiece instanceof Black) {
			this.name = "bp";
		}
		else {
			this.name = "wp";
		}
	}

	@Override
	/**
	 * Overrided implementation of movable method from <b>PieceFunctions</b> for <b>Pawn</b>
	 */
	public boolean movable(char hor, int vert) {
		if(hor == this.horizontal && vert == this.vertical){
			return false;
		}
		else{
			if(this.isBlack()){
				if(this.horizontal == hor){
					if(this.vertical - vert == 2 && this.moved == false && Board.board[Board.getVPos(this.vertical-1)][Board.getHPos(this.horizontal)] == null && Board.board[Board.getVPos(vert)][Board.getHPos(hor)] == null){
						return true;
					}
					else if(this.vertical - vert == 1 && Board.board[Board.getVPos(vert)][Board.getHPos(hor)] == null){
						return true;
					}
				}
				else{
					if(Math.abs(this.horizontal - hor) == 1 && (this.vertical - vert) == 1){
						if(Board.board[Board.getVPos(vert)][Board.getHPos(hor)] == null){
							if(this.enpassent(hor, vert) == true){
								return true;
							}
							else{
								return false;
							}
						}
						else if(!Board.board[Board.getVPos(vert)][Board.getHPos(hor)].sameColor(this.coloredPiece)){
							return true;
						}
					}
				}
			}
			else if(this.isWhite()){
				if(this.horizontal == hor){
					if(vert - this.vertical == 2 && this.moved == false && Board.board[Board.getVPos(this.vertical+1)][Board.getHPos(this.horizontal)] == null && Board.board[Board.getVPos(vert)][Board.getHPos(hor)] == null){
						return true;
					}
					else if(vert - this.vertical == 1 && Board.board[Board.getVPos(vert)][Board.getHPos(hor)] == null){
						return true;
					}
				}
				else{
					if(Math.abs(this.horizontal - hor) == 1 && (vert - this.vertical) == 1){
						if(Board.board[Board.getVPos(vert)][Board.getHPos(hor)] == null){
							if(this.enpassent(hor, vert) == true){
								return true;
							}
							else{
								return false;
							}
						}
						else if(!Board.board[Board.getVPos(vert)][Board.getHPos(hor)].sameColor(this.coloredPiece)){
							return true;
						}
					}
				}	
			}
		}
		return false;
	}
	
	/**
	 * Determines if the move is an enpassant and is a valid move
	 * 
	 * @param hor Horizontal <b>char</b> position of the piece on the board
	 * @param vert Vertical position of the piece on the board.
	 * @return true if an enpassant and valid move false if illegal
	 */
	public boolean enpassent(char hor, int vert){
		if(this.isBlack() && this.vertical == 4){
			if(Math.abs(hor - this.horizontal) == 1 && (this.vertical - vert == 1)){
				Piece supposedPawn = Board.board[Board.getVPos(vert+1)][Board.getHPos(hor)];
				if(supposedPawn instanceof Pawn && !supposedPawn.sameColor(this.coloredPiece) && ((Pawn) supposedPawn).doublestep == true ){
					Board.whitepArray.remove(Board.board[Board.getVPos(vert+1)][Board.getHPos(hor)]);
					Board.board[Board.getVPos(vert+1)][Board.getHPos(hor)] = null;
					return true;
				}
			}
		}
		else if(this.isWhite() && this.vertical == 5){
			if(Math.abs(hor - this.horizontal) == 1 && (vert - this.vertical == 1)){
				Piece supposedPawn = Board.board[Board.getVPos(vert-1)][Board.getHPos(hor)];
				if(supposedPawn instanceof Pawn && !supposedPawn.sameColor(this.coloredPiece) && ((Pawn) supposedPawn).doublestep == true ){
					Board.blackpArray.remove(Board.board[Board.getVPos(vert-1)][Board.getHPos(hor)]);
					Board.board[Board.getVPos(vert-1)][Board.getHPos(hor)] = null;
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Determines if the Pawn has reached the end of the board based on the instance
	 * of the <b>Player</b>
	 * 
	 * @return true if the Pawn is at the opponents end of the board, false otherwise
	 */
	public boolean reachedEnd() {
		if(this.isBlack() && this.vertical == 1){
			return true;
		}
		else if(this.isWhite() && this.vertical == 8){
			return true;
		}
		return false;
	}
	
	/**
	 * Promotes the Pawn at based on the <b>Key</b> constant that the parameter matches
	 * @param promotion <b>Key</b> constant returned by the <b>Parser</b>
	 */
	public void promote(int promotion) {
		if(promotion == Key.BISHOP) {
			Board.insertPiece(new Bishop(this.coloredPiece,
					this.horizontal, this.vertical),
						this.horizontal, this.vertical);
		}else if(promotion == Key.KNIGHT) {
			Board.insertPiece(new Knight(this.coloredPiece,
					this.horizontal, this.vertical),
						this.horizontal, this.vertical);
		}else if(promotion == Key.ROOK) {
			Board.insertPiece(new Rook(this.coloredPiece,
					this.horizontal, this.vertical),
						this.horizontal, this.vertical);
		}else {
			Board.insertPiece(new Queen(this.coloredPiece,
					this.horizontal, this.vertical),
						this.horizontal, this.vertical);
		}
	}
}
