/**
 * Main game is implemented here.
 * 
 * @author Daniel Kim
 * @author George Ding
 */

package chess;


import color.*;
import parser.Parser;
import pieces.Bishop;
import pieces.Piece;
import pieces.Queen;
import pieces.Rook;

import java.util.Scanner;

import key.Key;
import board.*;
import player.Player;

public class Chess {
	
	private static boolean turn = true;
	
	private static Player p1 = new Player(new White());
	private static Player p2 = new Player(new Black());
	
	public static void main(String args[]) {
		
		if(Board.getGameStatus() == false) {
			Board.setGameStatus(true);
			Board.setBoard();
		}
		
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
		Player currPlayer = null;
		
		int legal = 0;
		int init_draw = 0;
		
		while(Board.getGameStatus() == true) {
			
			if(Board.WhiteKing.inCheck || Board.BlackKing.inCheck){
				System.out.println("Check");
			}
			legal = 0;
			
			if(turn) {
				currPlayer = p1;
			}else {
				currPlayer = p2;
			}
			Board.displayBoard();
			
			char locLetter = 'a';
			int locNumber = 0;
			char destLetter = 'a';
			int destNumber =0;
			Piece tempPiece = null;
			Piece currentPiece = null;
			
			do {
				
				System.out.print("\n" + currPlayer.toString() + "'s move: ");
				String move = scan.nextLine();
				System.out.println();
				
				while(!Parser.checkErrors(move, turn)) {
					move = scan.nextLine();
				}
				
				String[] splitString = move.split(" ");
				
				if(splitString.length > 1) {
					locLetter = splitString[0].charAt(0);
					locNumber = Character.getNumericValue(splitString[0].charAt(1));
					destLetter = splitString[1].charAt(0);
					destNumber = Character.getNumericValue(splitString[1].charAt(1));
				
					tempPiece = Board.board[Board.getVPos(destNumber)][Board.getHPos(destLetter)];
					currentPiece = Board.board[Board.getVPos(locNumber)][Board.getHPos(locLetter)];
				}
				currPlayer.setMove(splitString);
				
				switch(Parser.parse(currPlayer.getMove())) {
				
					case Key.RESIGN:
						currPlayer = (currPlayer == p1) ? p2:p1;
						System.out.println(currPlayer.toString() + " wins");
						Board.setGameStatus(false);
						continue;
					case Key.DRAW:
						if(init_draw == 1) {
							Board.setGameStatus(false);
							legal = 1;
							continue;
						}
						break;
					case Key.INIT_DRAW:
						legal = currPlayer.makeMove(Key.INIT_DRAW);
						init_draw = 1;
						break;
					case Key.MOVE:
						init_draw = 0;
						legal = currPlayer.makeMove(Key.MOVE);
						break;
					
					case Key.KNIGHT:
						init_draw = 0;
						legal = currPlayer.makeMove(Key.KNIGHT);
						break;
					
					case Key.BISHOP:
						init_draw = 0;
						legal = currPlayer.makeMove(Key.BISHOP);
						break;
						
					case Key.ROOK:
						init_draw = 0;
						legal = currPlayer.makeMove(Key.ROOK);
						break;
						
					case Key.CASTLE:
						init_draw = 0;
						legal = currPlayer.makeMove(Key.CASTLE);
						break;
				}
				if(legal == 0) System.out.println("Illegal move, try again.");
				
			}while(legal == 0);
			
			//checkmate check here
			boolean checkmate = checkMate(Board.board[Board.getVPos(destNumber)][Board.getHPos(destLetter)]);
			
			if(checkmate){
				Board.setGameStatus(false);
				System.out.println("Checkmate");
				if(Board.WhiteKing.inCheck){
					System.out.println("Black Wins");
				}
				else{
					System.out.println("White Wins");
				}
				break;
			}
			
			if(turn && !checkmate) {
				boolean still = false;
				for(int i = 0; i < Board.blackpArray.size(); i++) {
					if(Board.blackpArray.get(i).movable(Board.WhiteKing.horizontal, Board.WhiteKing.vertical)) {
						still = true;
					}
				}
				if(!still) {
					Board.WhiteKing.inCheck = false;
				}
			}
			else if(!turn && !checkmate) {
				boolean still = false;
				for(int i = 0; i < Board.whitepArray.size(); i++) {
					if(Board.blackpArray.get(i).movable(Board.BlackKing.horizontal, Board.BlackKing.vertical)) {
						still = true;
					}
				}
				if(still) {
					Board.BlackKing.inCheck = false;
				}
			}
			
			if((Board.WhiteKing.inCheck == true && turn) || (Board.BlackKing.inCheck == true && !turn)) {
				Board.board[Board.getVPos(destNumber)][Board.getHPos(destLetter)] = tempPiece;
				Board.board[Board.getVPos(locNumber)][Board.getHPos(locLetter)] = currentPiece;
				System.out.println("Illegal move, try again.");
				
				if((Board.WhiteKing.inCheck == true && turn)) {
					if(tempPiece != null && !Board.blackpArray.contains(tempPiece)) {
						System.out.print(tempPiece.name);
						Board.blackpArray.add(tempPiece);
					}
				}
				else {
					if(tempPiece != null && !Board.whitepArray.contains(tempPiece)) {
						System.out.print(tempPiece.name);
						Board.whitepArray.add(tempPiece);
					}
				}
			}
			else {
				Board.board[Board.getVPos(destNumber)][Board.getHPos(destLetter)].checking = false;
				turn = !turn;
			}
		}
	}
	private static boolean checkMate(Piece checkingPiece){
		if(Board.WhiteKing.inCheck && !turn){
			for(int i = 1; i < 8; i++){
				for(char a = 'a'; a< 'i'; a++){
					if(Board.WhiteKing.movable(a, i)){
						return false;
					}
				}
			}
			for(int i = 0; i < Board.whitepArray.size(); i++){
				if(Board.whitepArray.get(i).movable(checkingPiece.horizontal, checkingPiece.vertical)){
					return false;
				}
			}
			if(checkingPiece instanceof Rook){
				if(checkingPiece.vertical == Board.WhiteKing.vertical){
					int vert = checkingPiece.vertical;
					int Kingvert = Board.WhiteKing.vertical;
					
					for(int i = 0; i < Board.whitepArray.size(); i++){
						while(Kingvert != vert){
							if(Kingvert > vert){
								Kingvert--;
							}
							else{
								Kingvert++;
							}
							if(!Board.whitepArray.get(i).movable(Board.WhiteKing.horizontal, Kingvert)){
								return true;
							}
						}
						if(!Board.whitepArray.get(i).movable(Board.WhiteKing.horizontal, Kingvert)){
							return true;
						}
					}
				}
				else if(checkingPiece.horizontal == Board.WhiteKing.horizontal){
					char hor = checkingPiece.horizontal;
					char Kinghor = Board.WhiteKing.horizontal;
					for(int i = 0; i < Board.whitepArray.size(); i++){
						while(Kinghor != hor){
							if(Kinghor > hor){
								Kinghor--;
							}
							else{
								Kinghor++;
							}
							if(!Board.whitepArray.get(i).movable(Kinghor, Board.WhiteKing.vertical)){
								return true;
							}
						}
						if(!Board.whitepArray.get(i).movable(Kinghor, Board.WhiteKing.vertical)){
							return true;
						}
					}
				}
			}
			else if(checkingPiece instanceof Bishop){
				for(int i = 0; i < Board.whitepArray.size(); i++){
					char loc = checkingPiece.horizontal;
					int dest = checkingPiece.vertical;
					
					char kingLoc = Board.WhiteKing.horizontal;
					int kingDest = Board.WhiteKing.vertical;
					
					while(loc != kingLoc){
						if(kingLoc > loc){
							kingLoc--;
						}
						else{
							kingLoc++;
						}
						if(kingDest > dest){
							kingDest--;
						}
						else{
							kingDest++;
						}
						if(!Board.whitepArray.get(i).movable(kingLoc, kingDest)){
							return true;
						}
					}
					if(!Board.whitepArray.get(i).movable(kingLoc, kingDest)){
						return true;
					}
				}
			}
			else if(checkingPiece instanceof Queen){
				if(checkingPiece.vertical == Board.WhiteKing.vertical){
					int vert = checkingPiece.vertical;
					int Kingvert = Board.WhiteKing.vertical;
					
					for(int i = 0; i < Board.whitepArray.size(); i++){
						while(Kingvert != vert){
							if(Kingvert > vert){
								Kingvert--;
							}
							else{
								Kingvert++;
							}
							if(!Board.whitepArray.get(i).movable(Board.WhiteKing.horizontal, Kingvert)){
								return true;
							}
						}
						if(!Board.whitepArray.get(i).movable(Board.WhiteKing.horizontal, Kingvert)){
							return true;
						}
					}
				}
				else if(checkingPiece.horizontal == Board.WhiteKing.horizontal){
					char hor = checkingPiece.horizontal;
					char Kinghor = Board.WhiteKing.horizontal;
					for(int i = 0; i < Board.whitepArray.size(); i++){
						while(Kinghor != hor){
							if(Kinghor > hor){
								Kinghor--;
							}
							else{
								Kinghor++;
							}
							if(!Board.whitepArray.get(i).movable(Kinghor, Board.WhiteKing.vertical)){
								return true;
							}
						}
						if(!Board.whitepArray.get(i).movable(Kinghor, Board.WhiteKing.vertical)){
							return true;
						}
					}
				}
				else{
					for(int i = 0; i < Board.whitepArray.size(); i++){
						char loc = checkingPiece.horizontal;
						int dest = checkingPiece.vertical;
						
						char kingLoc = Board.WhiteKing.horizontal;
						int kingDest = Board.WhiteKing.vertical;
						
						while(loc != kingLoc){
							if(kingLoc > loc){
								kingLoc--;
							}
							else{
								kingLoc++;
							}
							if(kingDest > dest){
								kingDest--;
							}
							else{
								kingDest++;
							}
							if(!Board.whitepArray.get(i).movable(kingLoc, kingDest)){
								return true;
							}
						}
						if(!Board.whitepArray.get(i).movable(kingLoc, kingDest)){
							return true;
						}
					}	
				}
			}
		}
		else if(Board.BlackKing.inCheck && turn){
			for(int i = 1; i < 8; i++){
				for(char a = 'a'; a< 'i'; a++){
					if(Board.BlackKing.movable(a, i)){
						return false;
					}
				}
			}
			for(int i = 0; i < Board.blackpArray.size(); i++){
				if(Board.blackpArray.get(i).movable(checkingPiece.horizontal, checkingPiece.vertical)){
					return false;
				}
			}
			if(checkingPiece instanceof Rook){
				if(checkingPiece.vertical == Board.BlackKing.vertical){
					int vert = checkingPiece.vertical;
					int Kingvert = Board.BlackKing.vertical;
					
					for(int i = 0; i < Board.blackpArray.size(); i++){
						while(Kingvert != vert){
							if(Kingvert > vert){
								Kingvert--;
							}
							else{
								Kingvert++;
							}
							if(!Board.blackpArray.get(i).movable(Board.BlackKing.horizontal, Kingvert)){
								return true;
							}
						}
						if(!Board.blackpArray.get(i).movable(Board.BlackKing.horizontal, Kingvert)){
							return true;
						}
					}
				}
				else if(checkingPiece.horizontal == Board.BlackKing.horizontal){
					char hor = checkingPiece.horizontal;
					char Kinghor = Board.BlackKing.horizontal;
					for(int i = 0; i < Board.blackpArray.size(); i++){
						while(Kinghor != hor){
							if(Kinghor > hor){
								Kinghor--;
							}
							else{
								Kinghor++;
							}
							if(!Board.blackpArray.get(i).movable(Kinghor, Board.BlackKing.vertical)){
								return true;
							}
						}
						if(!Board.blackpArray.get(i).movable(Kinghor, Board.BlackKing.vertical)){
							return true;
						}
					}
				}
			}
			else if(checkingPiece instanceof Bishop){
				for(int i = 0; i < Board.blackpArray.size(); i++){
					char loc = checkingPiece.horizontal;
					int dest = checkingPiece.vertical;
					
					char kingLoc = Board.BlackKing.horizontal;
					int kingDest = Board.BlackKing.vertical;
					
					while(loc != kingLoc){
						if(kingLoc > loc){
							kingLoc--;
						}
						else{
							kingLoc++;
						}
						if(kingDest > dest){
							kingDest--;
						}
						else{
							kingDest++;
						}
						if(!Board.blackpArray.get(i).movable(kingLoc, kingDest)){
							return true;
						}
					}
					if(!Board.blackpArray.get(i).movable(kingLoc, kingDest)){
						return true;
					}
				}
			}
			else if(checkingPiece instanceof Queen){
				if(checkingPiece.vertical == Board.BlackKing.vertical){
					int vert = checkingPiece.vertical;
					int Kingvert = Board.BlackKing.vertical;
					
					for(int i = 0; i < Board.blackpArray.size(); i++){
						while(Kingvert != vert){
							if(Kingvert > vert){
								Kingvert--;
							}
							else{
								Kingvert++;
							}
							if(!Board.blackpArray.get(i).movable(Board.BlackKing.horizontal, Kingvert)){
								return true;
							}
						}
						if(!Board.blackpArray.get(i).movable(Board.BlackKing.horizontal, Kingvert)){
							return true;
						}
					}
				}
				else if(checkingPiece.horizontal == Board.BlackKing.horizontal){
					char hor = checkingPiece.horizontal;
					char Kinghor = Board.BlackKing.horizontal;
					for(int i = 0; i < Board.blackpArray.size(); i++){
						while(Kinghor != hor){
							if(Kinghor > hor){
								Kinghor--;
							}
							else{
								Kinghor++;
							}
							if(!Board.blackpArray.get(i).movable(Kinghor, Board.BlackKing.vertical)){
								return true;
							}
						}
						if(!Board.blackpArray.get(i).movable(Kinghor, Board.BlackKing.vertical)){
							return true;
						}
					}
				}
				else{
					for(int i = 0; i < Board.blackpArray.size(); i++){
						char loc = checkingPiece.horizontal;
						int dest = checkingPiece.vertical;
						
						char kingLoc = Board.BlackKing.horizontal;
						int kingDest = Board.BlackKing.vertical;
						
						while(loc != kingLoc){
							if(kingLoc > loc){
								kingLoc--;
							}
							else{
								kingLoc++;
							}
							if(kingDest > dest){
								kingDest--;
							}
							else{
								kingDest++;
							}
							if(!Board.blackpArray.get(i).movable(kingLoc, kingDest)){
								return true;
							}
						}
						if(!Board.blackpArray.get(i).movable(kingLoc, kingDest)){
							return true;
						}
					}	
				}
			}
		}
		return false;
	}
}