/**
 * This class holds two parsing methods and an error check method for inputs.
 * Parsing is strictly done with this class and is heavily dependent on <b>Key</b> class
 * to determine certain moves.
 * 
 * @author Daniel Kim
 * @author George Ding
 */

package parser;

import board.Board;
import key.*;
import pieces.Piece;
import pieces.Rook;

public class Parser {
	
	private Parser() {};
	
	/**
	 * This is used for parsing the initial input from the player in the console to determine
	 * the move that the player is attempting to make.
	 * It checks through a split String, which is the original String split on spaces.
	 * 
	 * @param split Input String split on spaces.
	 * @return return values are based on <b>Key</b> constants
	 * <p><b>Key</b>.<em>INIT_DRAW</em> if input matches requirements for initiating a draw</p>
	 * <p><b>Key</b>.<em>KNIGHT</em> if input matches requirements for an underpromotion to <b>Knight</b></p>
	 * <p><b>Key</b>.<em>ROOK</em> if input matches requirements for an underpromotion to <b>Rook</b></p>
	 * <p><b>Key</b>.<em>BISHOP</em> if input matches requirements for and underpromotion to <b>Bishop</b></p>
	 * <p><b>Key</b>.<em>CASTLE</em> if input matches requirements for castling</p>
	 * <p><b>Key</b>.<em>MOVE</em> if input matches requirements for moving a piece. Also the same key for promotion to a <b>Queen</b> if valid</p>
	 * <p><b>Key</b>.<em>DRAW</em> if input matches requirements for accepting a draw</p>
	 * <p><b>Key</b>.<em>RESIGN</em> if input matches requirements for player resigning</p>
	 * <p>returns -1 if none of the cases match which would signify an invalid move</p>
	 */
	public static int parse(String[] split) {
		
		if(split.length>2) {
			switch (split[2]) {
		
				case "draw?": return Key.INIT_DRAW;
				case "N":	return Key.KNIGHT;
				case "R":	return Key.ROOK;
				case "B":	return Key.BISHOP;
			}
		}else if(split.length == 2) { 
			if(split[0].equals("e1") && split[1].equals("g1") ||
				(split[0].equals("e1") && split[1].equals("c1")) ||
					(split[0].equals("e8") && split[1].equals("g8")) ||
						(split[0].equals("e8") && split[1].equals("c8"))) {
				return Key.CASTLE;
			}
			return Key.MOVE; 
		}
		else if(split[0].equals("draw")) { return Key.DRAW; }
		else if(split[0].equals("resign")) {return Key.RESIGN; }
		return -1;
	}
	
	/**
	 * This is specifically used for parsing the movement of the piece in the given input.
	 * 
	 * @param moveSet The split String of the user input
	 * @param key The <b>Key</b> constant determined by <code>parse(String[])</code>
	 * @return returns the 2D array representation of the move given
	 */
	public static int[][] parseMove(String[] moveSet, int key){
		
		return new int[][] {{Board.getHPos(moveSet[0].charAt(0)), 
				(Character.getNumericValue(moveSet[0].charAt(1)))},
			{Board.getHPos(moveSet[1].charAt(0)),
				(Character.getNumericValue(moveSet[1].charAt(1)))},
			{key}};
	}
	
	/**
	 * Checks for input errors in the initial user input String
	 * @param arg User input String
	 * @param turn The <b>current</b> player's turn
	 * @return true if valid string, false otherwise. Prints usage string "Illegal move, try again."
	 */
	public static boolean checkErrors(String arg, boolean turn) {
		String[] stringArray = arg.split(" ");
		
		if(stringArray.length > 3) {
			System.out.println("Illegal move, try again.");
			return false;
		}
		
		char locLetter;
		int locNumber;
		char destLetter;
		int destNumber;
		
		switch(stringArray[0]) {
		
		case "castle":
			locLetter = stringArray[1].charAt(0);
			locNumber = Character.getNumericValue(stringArray[1].charAt(1));
			
			if(locLetter >= 'a' && locLetter <= 'h' && locNumber >= 1 && locNumber <= 8) {
				if(Board.board[Board.getVPos(locNumber)][Board.getHPos(locLetter)] instanceof Rook) {
					Piece rook = Board.board[Board.getVPos(locNumber)][Board.getHPos(locLetter)];
					if((rook.isBlack() && turn == false) || (rook.isWhite() && turn == true)) {
						return true;
					}
				}
			}
			System.out.println("Illegal move, try again.");
			return false;
		
		case "draw": return true;
		case "resign":
			return true;
			
		default: 
			
			if(stringArray[0].length() != 2 || stringArray[1].length() != 2) {
				System.out.println("Illegal move, try again.");
				return false;
			}
			else {
				locLetter = stringArray[0].charAt(0);
				locNumber = Character.getNumericValue(stringArray[0].charAt(1));
				destLetter = stringArray[1].charAt(0);
				destNumber = Character.getNumericValue(stringArray[1].charAt(1));
				
				if(locLetter >= 'a' && locLetter <= 'h' && locNumber >= 1 && locNumber <= 8 && destLetter >= 'a' && destLetter <= 'h' && destNumber >= 1 && destNumber <= 8) {
					if(Board.board[Board.getVPos(locNumber)][Board.getHPos(locLetter)] != null) {
						Piece current = Board.board[Board.getVPos(locNumber)][Board.getHPos(locLetter)];
						if((current.isBlack() && turn == false) || (current.isWhite() && turn == true)) {
							return true;
						}
					}
				}	
			}
			System.out.println("Illegal move, try again.");
			return false;
		}
		
	}
}
