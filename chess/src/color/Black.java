/**
 * 
 * Used for instanceof <b>Colors</b>
 * <b>'Black'</b> instance of <b>Colors</b>
 * 
 * @author Daniel Kim
 * @author George Ding
 */

package color;

public class Black extends Colors{

}
