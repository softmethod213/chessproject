/**
 * 
 * Used for instanceof <b>Colors</b>
 * <b>'White'</b> instance of <b>Colors</b>
 * 
 * @author Daniel Kim
 * @author George Ding
 */

package color;

public class White extends Colors{

}
