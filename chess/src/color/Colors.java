/**
 * 
 * Color class specifically for abstraction of colors.
 * 'White' and 'Black' subclass created for creating instances of <b>Colors</b>
 * @author Daniel Kim
 * @author George Ding
 */

package color;

public abstract class Colors {

}
