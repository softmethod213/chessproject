/**
 * Used for player moves. Contains constants that represent different player moves
 * and pawn promotion/underpromotions
 * 
 * @author Daniel Kim
 * @author George Ding
 */

package key;

public class Key{
	
	public final static int RESIGN = 0;
	public final static int DRAW = 1;
	public static final int INIT_DRAW = 2;
	public static final int KNIGHT = 5;
	public static final int ROOK = 6;
	public static final int BISHOP = 7;
	public static final int MOVE = 4;
	public static final int CASTLE = 3;
	
}
